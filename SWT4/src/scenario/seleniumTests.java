import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class seleniumTests {
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		driver.get("http://www.bitbucket.org/");
		assertEquals(driver.getTitle(),"Free source code hosting for Git and Mercurial by Bitbucket");
		driver.findElement(By.cssSelector("a[class='aui-nav-link login-link']")).click();
		try {driver.wait(3000); } catch (Exception e) {};
	}

	@After
	public void tearDown() throws Exception {
		assertEquals(driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"),username);
		driver.findElement(By.id("user-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[href='/account/signout/']")).click();
		assertEquals("Free source code hosting for Git and Mercurial by Bitbucket",driver.getTitle());
		driver.quit();
	}

	@Test
	public void testIssueTrackerScenario1() {
		driver.findElement(By.id("id_username")).sendKeys(username);
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		assertEquals(driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"),username);
		
		//CREATE
		driver.findElement(By.id("repositories-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[title='User1-swt4/test_repo']")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo � Bitbucket",driver.getTitle());
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {System.out.println("heh??");};
		
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");		
		driver.findElement(By.cssSelector("a[class='chzn-single chzn-default']")).click();
		driver.findElement(By.cssSelector("li[data-value='User2-swt4']")).click();		
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1")).isDisplayed());
		
		//LOGOUT
		driver.findElement(By.id("user-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[href='/account/signout/']")).click();
		assertEquals("Free source code hosting for Git and Mercurial by Bitbucket",driver.getTitle());
		
		//log in as user2		
		driver.findElement(By.cssSelector("a[class='aui-nav-link login-link']")).click();
		try {driver.wait(3000); } catch (Exception e) {};
		driver.findElement(By.id("id_username")).sendKeys("User2-swt4");
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};	
		assertEquals("User2-swt4",driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"));
		driver.findElement(By.id("repositories-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[title='User1-swt4/test_repo']")).click();
		try {driver.wait(2000); } catch (Exception e) {};
		
		//DELETE
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.partialLinkText("selenium_test1")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.linkText("More")).click();
		driver.findElement(By.id("issue-delete-link")).click();
		driver.switchTo().alert().accept();
		
		assertTrue(driver.findElements(By.partialLinkText("selenium_test1")).isEmpty());
	}
	
	@Test
	public void testIssueTrackerScenario2()
	{
		driver.findElement(By.id("id_username")).sendKeys(username);
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		assertEquals(driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"),username);
		driver.findElement(By.id("repositories-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[title='User1-swt4/test_repo']")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo � Bitbucket",driver.getTitle());
		
		//Create Bug 1
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1_b1");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1_b1")).isDisplayed());
		
		//Create Bug 2
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1_b2");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1_b2")).isDisplayed());
		
		//Create Proposal 1
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1_p");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");
		driver.findElements(By.cssSelector("a[class='chzn-single']")).get(1).click();
		driver.findElement(By.id("id_kind_chzn_o_2")).click();
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1_p")).isDisplayed());
		
		//filter
		driver.findElement(By.id("repo-issues-link")).click();
		driver.findElement(By.className("icon bug")).click();
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1_b1")).isDisplayed());
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1_b2")).isDisplayed());
		driver.findElement(By.cssSelector("a[href='/User1-swt4/test_repo/issues']")).click();
		
		//Delete
		driver.findElement(By.partialLinkText("selenium_test1_b1")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.linkText("More")).click();
		driver.findElement(By.id("issue-delete-link")).click();
		driver.switchTo().alert().accept();
		
		driver.findElement(By.partialLinkText("selenium_test1_b2")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.linkText("More")).click();
		driver.findElement(By.id("issue-delete-link")).click();
		driver.switchTo().alert().accept();
		
		driver.findElement(By.partialLinkText("selenium_test1_p")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.linkText("More")).click();
		driver.findElement(By.id("issue-delete-link")).click();
		driver.switchTo().alert().accept();
	}

	@Test
	public void testIssueTrackerScenario3()
	{
		driver.findElement(By.id("id_username")).sendKeys(username);
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		assertEquals(driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"),username);
		driver.findElement(By.id("repositories-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[title='User1-swt4/test_repo']")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo � Bitbucket",driver.getTitle());
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");
		driver.findElement(By.cssSelector("a[class='chzn-single chzn-default']")).click();
		driver.findElement(By.cssSelector("li[data-value='User2-swt4']")).click();
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1")).isDisplayed());
		
		//EDIT
		driver.findElement(By.partialLinkText("selenium_test1")).click();
		try {driver.wait(2000); } catch (Exception e) {};
		driver.findElement(By.id("edit-issue")).click();
		try {driver.wait(2000); } catch (Exception e) {};
		driver.findElement(By.id("id_title")).clear();
		driver.findElement(By.id("id_title")).sendKeys("selenium_test2");
		driver.findElement(By.id("issue-form")).submit();
		assertEquals("test2",driver.findElement(By.id("issue-title")).getText());
		
		//RESOLVE
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.partialLinkText("selenium_test2")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.id("change-state")).click();
		driver.findElement(By.cssSelector("button[class='button-panel-button aui-button transition-issue-button']")).click();
		
		//VERIFY
		driver.findElement(By.id("repo-issues-link")).click();
		assertTrue(driver.findElements(By.partialLinkText("selenium_test2")).isEmpty());
	}
	
	@Test
	public void testIssueTrackerScenario4()
	{
		driver.findElement(By.id("id_username")).sendKeys(username);
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		assertEquals(driver.findElement(By.id("user-dropdown-trigger")).getAttribute("title"),username);
		
		//CREATE
		driver.findElement(By.id("repositories-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[title='User1-swt4/test_repo']")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo � Bitbucket",driver.getTitle());
		driver.findElement(By.id("repo-issues-link")).click();
		try {
			driver.findElement(By.linkText("Create your first issue")).click(); 
		} catch (Exception e) {
			driver.findElement(By.linkText("Create issue")).click();
		}
		try {driver.wait(3000); } catch (Exception e) {};
		assertEquals("User1-swt4 / test_repo / issues / new � Bitbucket",driver.getTitle());
		driver.findElement(By.id("id_title")).sendKeys("selenium_test1");
		driver.findElement(By.id("id_content")).sendKeys("test1 text");
		driver.findElement(By.cssSelector("a[class='chzn-single chzn-default']")).click();
		driver.findElement(By.cssSelector("li[data-value='User2-swt4']")).click();
		driver.findElement(By.id("issue-form")).submit();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.partialLinkText("selenium_test1")).isDisplayed());

		//logout
		driver.findElement(By.id("user-dropdown-trigger")).click();
		driver.findElement(By.cssSelector("a[href='/account/signout/']")).click();
		assertEquals("Free source code hosting for Git and Mercurial by Bitbucket",driver.getTitle());
		
		//log in as user2
		driver.findElement(By.cssSelector("a[class='aui-nav-link login-link']")).click();
		try {driver.wait(3000); } catch (Exception e) {};
		driver.findElement(By.id("id_username")).sendKeys("User2-swt4");
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		driver.findElement(By.id("user-dropdown-trigger")).click();
		driver.findElement(By.id("inbox-link")).click();
		
		//click the first message
		driver.findElements(By.className("execute")).get(0).click();
		//get all paragraphs in message
		List<WebElement> array = driver.findElement(By.tagName("article")).findElements(By.tagName("p"));
		//compare the last paragraph.
		assertEquals("Responsible: User2-swt4",array.get(array.size()-1).getText());
		try {driver.wait(2000); } catch (Exception e) {};
		
		//DELETE
		driver.findElement(By.id("repo-issues-link")).click();
		try {driver.wait(1000); } catch (Exception e) {};
		driver.findElement(By.partialLinkText("selenium_test1")).click();
		try {driver.wait(1500); } catch (Exception e) {};
		driver.findElement(By.linkText("More")).click();
		driver.findElement(By.id("issue-delete-link")).click();
		driver.switchTo().alert().accept();
		
		assertTrue(driver.findElements(By.partialLinkText("selenium_test1")).isEmpty());
	}
	
	@Test
	public void testIssueTrackerScenario5()
	{
		password = "wrongpassword";
		driver.findElement(By.id("id_username")).sendKeys(username);
		driver.findElement(By.id("id_password")).sendKeys(password);
		driver.findElement(By.id("login-form")).submit();
		try {driver.wait(2000); } catch (Exception e) {};
		assertTrue(driver.findElement(By.className("error")).findElement(By.tagName("div")).getText().contains("Invalid"));
		driver.quit();
	}
	
	public String username = "User1-swt4";
	public String password = "softwaretesting";
	public WebDriver driver;
}