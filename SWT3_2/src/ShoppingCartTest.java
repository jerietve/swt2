import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ShoppingCartTest {

	Product product1 = new Product("Chair", 100);
	Product product2 = new Product("Table", 200);
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testShoppingCart() {
		// Test constructor by checking that no exception occurs
		try {
			ShoppingCart shoppingCart = new ShoppingCart();
		} catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testGetBalanceNoProducts() {
		// Add two items to a shopping cart and compare expected balance with balance returned
		// from the ShoppingCart.getBalance() method with zero items, one item and two items in the shopping cart
		// (so that for the for-loop 0, 1 and many times are tested, otherwise CodeCover will complain)
		ShoppingCart shoppingCart = new ShoppingCart();
		assertEquals(0, shoppingCart.getBalance(), 0.000001d);
	}
	
	@Test
	public void testGetBalanceOneProduct() {
		// Add two items to a shopping cart and compare expected balance with balance returned
		// from the ShoppingCart.getBalance() method with zero items, one item and two items in the shopping cart
		// (so that for the for-loop 0, 1 and many times are tested, otherwise CodeCover will complain)
		ShoppingCart shoppingCart = new ShoppingCart();
		
		shoppingCart.addItem(product1);
		assertEquals(100, shoppingCart.getBalance(), 0.000001d);
	}
	
	@Test
	public void testGetBalanceTwoProducts() {
		// Add two items to a shopping cart and compare expected balance with balance returned
		// from the ShoppingCart.getBalance() method with zero items, one item and two items in the shopping cart
		// (so that for the for-loop 0, 1 and many times are tested, otherwise CodeCover will complain)
		ShoppingCart shoppingCart = new ShoppingCart();
		
		shoppingCart.addItem(product1);		
		shoppingCart.addItem(product2);
		assertEquals(300, shoppingCart.getBalance(), 0.000001d);
	}

	@Test
	public void testAddItem() {
		// Add one item to a shopping cart and check that
		// the item count has increased by 1 (use ShoppingCart.getItemCount() for this)
		ShoppingCart shoppingCart = new ShoppingCart();
		
		int countBefore = shoppingCart.getItemCount();
		shoppingCart.addItem(product1);
		
		assertEquals(countBefore + 1, shoppingCart.getItemCount());
	}

	@Test
	public void testRemoveItem() {
		// Remove one item from shopping cart containing one item
		// Check that item count has decreased by one (use getItemCount() again)
		// After that also remove an item that the shopping cart does not contain
		// to test the exception
		ShoppingCart shoppingCart = new ShoppingCart();
		shoppingCart.addItem(product1);
		
		int countBefore = shoppingCart.getItemCount();
		
		try {
			shoppingCart.removeItem(product1);
		} catch (ProductNotFoundException e) {
			fail(e.getMessage());
		}
		
		assertEquals(countBefore - 1, shoppingCart.getItemCount());
	}
	
	@Test
	public void testRemoveNonExistingItem() {
		// Remove an item that the shopping cart does not contain
		// to test the exception
		ShoppingCart shoppingCart = new ShoppingCart();
		
		int countBefore = shoppingCart.getItemCount();
		
		try {
			shoppingCart.removeItem(null);
			//fail("Should throw exception, but not thrown");
		} catch (ProductNotFoundException e) {
		}
		
		assertEquals(countBefore, shoppingCart.getItemCount());		// Should be the same number of items
	}

	@Test
	public void testGetItemCount() {
		// It is assumed that a newly created shopping cart is empty
		// Check that the number returned by getItemCount() is indeed zero
		ShoppingCart shoppingCart = new ShoppingCart();
		assertEquals(0, shoppingCart.getItemCount());
	}

	@Test
	public void testEmpty() {
		// Empty shopping cart and test is item count is zero
		ShoppingCart shoppingCart = new ShoppingCart();
		
		shoppingCart.addItem(product1);
		shoppingCart.empty();
		
		assertEquals(0, shoppingCart.getItemCount());
	}

}
