import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ProductTest {	
	// Test values
	private final static String productTitle = "Book";
	private final static double negativeProductPrice = -9.95;
	private final static double positiveProductPrice = 29.95;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProduct() {
		try {
			new Product(productTitle, negativeProductPrice);
		} catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testGetTitle() {
		Product product1 = new Product(productTitle, positiveProductPrice);
		
		assertEquals(productTitle, product1.getTitle());
	}

	@Test
	public void testGetPrice() {
		Product product1 = new Product(productTitle, negativeProductPrice);
		
		assertEquals(negativeProductPrice, product1.getPrice(), 0.000001d);
	}
	
	@Test
	public void testEqualsObject() {
		Product product1		= new Product(productTitle, positiveProductPrice);
		Product sameAsProduct1	= new Product(productTitle, positiveProductPrice);
		
		assertTrue(product1.equals(sameAsProduct1));
	}
	
	@Test
	public void testEqualsObjectNotEqual() {
		Product product1		= new Product(productTitle, positiveProductPrice);
		String fakeProduct		= "blaaoiwejf";
		
		assertFalse(product1.equals(fakeProduct));
	}

}
