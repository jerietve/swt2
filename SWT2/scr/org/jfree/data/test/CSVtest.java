package org.jfree.data.test;

import junit.framework.TestCase;
import org.jfree.data.io.CSV;
import java.io.*;

import java.util.Random;


public class CSVtest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
		
		stringEmpty = "";
		readerEmpty = new StringReader(stringEmpty);
		
		stringUnicode = "";
		int i;
		Random rand; //random unicode.
		for (i = 0; i < 1000; i++)
		{
			rand = new Random();
			stringUnicode += Integer.toHexString(rand.nextInt(0x1000) + 0x1000);
		}
		readerUnicode = new StringReader(stringUnicode);
		
		stringSimpleFile = "";
		for (i=0;i<500;i++)
		{
			rand = new Random();
			if (i % 5 == 0 && i % 25 != 0) stringSimpleFile += " ";
			else if (i % 25 == 0) stringSimpleFile += "\n";
			else stringSimpleFile += (char)rand.nextInt(26) + 'a';
		}
		readerSimpleFile = new StringReader(stringSimpleFile);

	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	private Reader readerEmpty;
	private Reader readerUnicode;
	private Reader readerRandomNonText;
	private Reader readerSimpleFile;
	private Reader readerEmptyCells;
	private Reader readerRowsDiffLength;
	
	private CSV testCSV = new CSV();
	
	private String stringEmpty;
	private String stringUnicode;
	private String stringRandomNonText;
	private String stringSimpleFile;
	private String stringEmptyCells;
	private String stringRowsDiffLength;
}
