package org.jfree.data.test;

import org.jfree.data.Range;

import junit.framework.TestCase;

public class RangeTest extends TestCase
{	
	// Input bounds for testing purposes (just integers, no Range object)
    private double[]	negativeBounds,
    					zeroSpanningBounds,
    					positiveBounds,
    					sameBounds;
    
    // Ranges based on bounds above
    private Range		negativeRange,
						zeroSpanningRange,
						positiveRange,
						sameRange;

    protected void setUp() throws Exception
    {
       // Initialize bounds by taking a random range for each equivalence partition
    	negativeBounds		= new double[] {-3, -1};
    	zeroSpanningBounds	= new double[] {-7, 5};
    	positiveBounds		= new double[] {0, 5789032.27};
    	sameBounds			= new double[] {Math.PI, Math.PI};

    	// Initialize Range objects using the bounds above
    	negativeRange		= new Range(negativeBounds[0], negativeBounds[1]);
    	zeroSpanningRange	= new Range(zeroSpanningBounds[0], zeroSpanningBounds[1]);
    	positiveRange		= new Range(positiveBounds[0], positiveBounds[1]);
    	sameRange			= new Range(sameBounds[0], sameBounds[1]);
    	
    }

    protected void tearDown() throws Exception
    {
    	negativeBounds		= null;
    	zeroSpanningBounds	= null;
    	positiveBounds		= null;
    	sameBounds			= null;
    	
    	negativeRange		= null;
    	zeroSpanningRange	= null;
    	positiveRange		= null;
    	sameRange			= null;
    }

    public void testCombine() 
    {
        try { assertEquals("Negative/null", Range.combine(new Range(-100,  -10), null), new Range(-100, -10)); }
        catch(Exception e) { fail("Exception caught where not expected: Negative/null" + e.getMessage()); }
        try { assertEquals("Null/singular", Range.combine(null, new Range(10, 10)), new Range(10, 10)); }
        catch(Exception e) { fail("Exception caught where not expected: Null/singular" + e.getMessage()); }
        try { assertEquals("Null/null", Range.combine(null, null), null); }
        catch(Exception e) { fail("Exception caught where not expected: Null/null" + e.getMessage()); }
        try { assertEquals("Negative/positive, r1ub < r2lb", Range.combine(new Range(-100,  -10), new Range(10, 100)), new Range(-100, 100)); }
        catch(Exception e) { fail("Exception caught where not expected: Negative/positive, r1ub < r2lb\n" + e.getMessage()); }
        try { assertEquals("Singular/positive, r1ub == r2lb", Range.combine(new Range(-3,  -3), new Range(-3, -1)), new Range(-3, -1)); }
        catch(Exception e) { fail("Exception caught where not expected: Singular/positive, r1ub == r2lb\n" + e.getMessage()); }
        try { assertEquals("Spanning/positive, r1lb < r2lb, r1ub > r2lb, r1ub < r2ub", Range.combine(new Range(-10, 100), new Range(90, 1000)), new Range(-10, 1000)); }
        catch(Exception e) { fail("Exception caught where not expected: Spanning/positive, r1lb < r2lb, r1ub > r2lb, r1ub < r2ub\n" + e.getMessage()); }
        try { assertEquals("Singular/spanning, r1lb < r2lb, r1ub == r2ub", Range.combine(new Range(3.72, 3.72), new Range(-10E18, 3.72)), new Range(-10E18, 3.72)); }
        catch(Exception e) { fail("Exception caught where not expected: Singular/spanning, r1lb < r2lb, r1ub == r2ub\n" + e.getMessage()); }
        try { assertEquals("Spanning/negative, r1lb < r2lb, r1ub > r2ub", Range.combine(new Range(-10E17, 10E17), new Range(-10E16, -10E15)), new Range(-10E17, -10E15)); }
        catch(Exception e) { fail("Exception caught where not expected: Spanning/negative, r1lb < r2lb, r1ub > r2ub\n" + e.getMessage()); }
        try { assertEquals("Positive/positive, r1lb == r2lb, r1ub < r2ub", Range.combine(new Range(Math.E, Math.PI), new Range(Math.E, 10.3)), new Range(Math.E, 10.3)); }
        catch(Exception e) { fail("Exception caught where not expected: Positive/positive, r1lb == r2lb, r1ub < r2ub\n" + e.getMessage()); }
        try { assertEquals("Singular/singular, r1lb == r2lb, r1ub == r2ub", Range.combine(new Range(0, 0), new Range(0.0, 0.0)), new Range(0, 0)); }
        catch(Exception e) { fail("Exception caught where not expected: Singular/singular, r1lb == r2lb, r1ub == r2ub\n" + e.getMessage()); }
        try { assertEquals("Spanning/Spanning, r1lb == r2lb, r1ub > r2ub", Range.combine(new Range(-0.1, 0.1), new Range(-0.1, 0.0001)), new Range(-0.1, 0.0001)); }
        catch(Exception e) { fail("Exception caught where not expected: Spanning/Spanning, r1lb == r2lb, r1ub > r2ub\n" + e.getMessage()); }
        try { assertEquals("Negative/negative, r1lb > r2lb, r1ub < r2ub", Range.combine(new Range(-10, -2), new Range(-3000, -0.0001)), new Range(-3000, -0.0001)); }
        catch(Exception e) { fail("Exception caught where not expected: Negative/negative, r1lb > r2lb, r1ub < r2ub\n" + e.getMessage()); }
        try { assertEquals("Spanning/spanning, r1lb > r2lb, r1ub == r2ub", Range.combine(new Range(-10, 2), new Range(-3, 2)), new Range(-10, 2)); }
        catch(Exception e) { fail("Exception caught where not expected: Spanning/spanning, r1lb > r2lb, r1ub == r2ub\n" + e.getMessage()); }
        try { assertEquals("Negative/negative, r1lb > r2lb, r1lb < r2ub, r1ub > r2ub", Range.combine(new Range(-10, -2), new Range(-3, -1)), new Range(-10, -1)); }
        catch(Exception e) { fail("Exception caught where not expected: Negative/negative, r1lb > r2lb, r1lb < r2ub, r1ub > r2ub\n" + e.getMessage()); }
        try { assertEquals("Singular/positive, r1lb == r2ub", Range.combine(new Range(3, 3), new Range(3, 10)), new Range(3, 10)); }
        catch(Exception e) { fail("Exception caught where not expected: Singular/positive, r1lb == r2ub\n" + e.getMessage()); }
        try { assertEquals("Positive/positive, r1lb > r2ub", Range.combine(new Range(3, 4), new Range(10, 100)), new Range(3, 100)); }
        catch(Exception e) { fail("Exception caught where not expected: Positive/positive, r1lb > r2ub\n" + e.getMessage()); }
    }
    
    public void testContains()
    {
    	double[][] testRanges = { negativeBounds, zeroSpanningBounds, positiveBounds, sameBounds };
    	
    	// Test four type of ranges
    	for(int i = 0; i < 4; i++) {
    		double lowerBound = testRanges[i][0];
    		double upperBound = testRanges[i][1];
    		Range range = new Range(lowerBound, upperBound);
    		
    		assertFalse("Just below the lower bound", range.contains(lowerBound - 1));
    		assertFalse("Just above the upper bound", range.contains(upperBound + 1));
    		
    		// It is assumed that the lower and upper bounds are included in the range
    		assertTrue("The lower bound", range.contains(lowerBound));
    		assertTrue("The upper bound", range.contains(upperBound));

    		// Do not test the following boundary values, for a range that contains only one value
    		if(lowerBound != upperBound) {
	    		assertTrue("Just above the lower bound", range.contains(lowerBound + 1));
	    		assertTrue("Somewhere in the middle", range.contains((lowerBound + upperBound) / 2));
	    		assertTrue("Just below the upper bound", range.contains(upperBound - 1));
    		}
    	}
    }
    
    public void testEquals() {
    	Range zeroSpanningDifferentSign	= new Range(zeroSpanningBounds[0], -zeroSpanningBounds[1]);
    	Range sameRangeLowerbound		= new Range(sameBounds[0], sameBounds[0] + 1);
    	Range otherPositiveRange		= new Range(positiveBounds[0] + 1, positiveBounds[1] + 1);
    	Range slightlyShiftedNegative	= new Range(negativeBounds[0] + Double.MIN_VALUE, negativeBounds[1] + Double.MIN_VALUE);
    	
    	assertFalse("Positive range taking negative range", positiveRange.equals(negativeRange));
    	assertFalse("Range spanning zero taking same range, but with different upper bound sign",
    			positiveRange.equals(zeroSpanningDifferentSign));

    	class NotARealRange
    	{
    	    public double upper, lower;
    	    
    	    public NotARealRange(double lower, double upper)
    	    {
    	        this.upper = upper;
    	        this.lower = lower;
    	    }
    	}
    	
    	assertFalse("Negative range and other class instance", new Range(-3,  -2).equals(new NotARealRange(-3, -2)));    	
    	assertFalse("Positive range, input null", positiveRange.equals(null));
    	assertFalse("Range with one value taking range with this value as lower bound, upperbound greater",
    			sameRange.equals(sameRangeLowerbound));
    	assertFalse("Positive range taking another range with the same length", sameRange.equals(otherPositiveRange));
    	assertFalse("Negative range taking copy that is slightly shifted towards positive infinity",
    			negativeRange.equals(slightlyShiftedNegative));
    }
    
    public void testLength() {
    	assertEquals("Completly negative bounds", negativeBounds[1] - negativeBounds[0], negativeRange.getLength(), 0.000001d);
    	assertEquals("Zero spanning bounds", zeroSpanningBounds[1] - zeroSpanningBounds[0], zeroSpanningRange.getLength(), 0.000001d);
    	assertEquals("Completly positive bounds", positiveBounds[1] - positiveBounds[0], positiveRange.getLength(), 0.000001d);
    	assertEquals("Same bounds", sameBounds[1] - sameBounds[0], sameRange.getLength(), 0.000001d);
    }
    
    public void testIntersects() {
    	Range negativeSingular = new Range(negativeBounds[0], negativeBounds[0]);
    	
    	assertFalse("Negative/positive, r1ub < r2lb", negativeRange.intersects(positiveBounds[0], positiveBounds[1]));
    	assertTrue("Singular/positive, r1ub == r2lb", negativeSingular.intersects(negativeBounds[0], negativeBounds[1]));
        assertTrue("Spanning/positive, r1lb < r2lb, r1ub > r2lb, r1ub < r2ub", new Range(-10, 100).intersects(90, 1000));
        assertTrue("Singular/spanning, r1lb < r2lb, r1ub == r2ub", new Range(3.72, 3.72).intersects(-10E18, 3.72));
        assertTrue("Spanning/negative, r1lb < r2lb, r1ub > r2ub", new Range(-10E17, 10E17).intersects(-10E16, -10E15));
        assertTrue("Positive/positive, r1lb == r2lb, r1ub < r2ub", new Range(Math.E, Math.PI).intersects(Math.E, 10.3));
        assertTrue("Singular/singular, r1lb == r2lb, r1ub == r2ub", new Range(0, 0).intersects(0.0, 0.0));
        assertTrue("Spanning/Spanning, r1lb == r2lb, r1ub > r2ub", new Range(-0.1, 0.1).intersects(-0.1, 0.0001));
        assertTrue("Negative/negative, r1lb > r2lb, r1ub < r2ub", new Range(-10, -2).intersects(-3000, -0.0001));
        assertTrue("Spanning/spanning, r1lb > r2lb, r1ub == r2ub", new Range(-10, 2).intersects(-3, 2));
        assertTrue("Negative/negative, r1lb > r2lb, r1lb < r2ub, r1ub > r2ub", new Range(-10, -2).intersects(-3, -1));
        assertTrue("Singular/positive, r1lb == r2ub", new Range(3, 3).intersects(3, 10));
        assertFalse("Positive/positive, r1lb > r2ub", new Range(3, 4).intersects(10, 100));
    }
    
    public void testShift() {
    	/* Test combinations generated by ACTS
    	
    	base		delta						allowZeroCrossing
    	----------------------------------------------------------------
		negative	zero						false
		spanning	zero						true
		positive	zero						false
		singular	zero						false
		negative	positive					true
		spanning	positive					false
		positive	positive					true
		singular	positive					true
		negative	negative					false
		spanning	negative					true
		positive	negative					true
		singular	negative					true
		negative	partly crossing zero		false
		spanning	partly crossing zero		true
		positive	partly crossing zero		false
		singular	partly crossing zero		true -> this is impossible, replaced singular by positive
		negative	completely crossing zero	false
		spanning	completely crossing zero	true
		positive	completely crossing zero	false
		singular	completely crossing zero	true
		
		*/

        assertEquals("Negative,  zero, false", Range.shift(new Range(-3, -1), 0, false), new Range(-3, -1));
        assertEquals("Spanning, zero, true", Range.shift(new Range(-10, 10), 0, true), new Range(-10, 10));
        assertEquals("Positive, zero, false", Range.shift(new Range(10, 100), 0, false), new Range(10, 100));
        assertEquals("Singular, zero, false", Range.shift(new Range(0, 0), 0, false), new Range(0, 0));
        assertEquals("Negative, positive, true", Range.shift(new Range(-100, -10), 10, true), new Range(-90, 0));
        assertEquals("Spanning, positive, false", Range.shift(new Range(-10, 10), 5, false), new Range(-5, 15));
        assertEquals("Positive, positive, true", Range.shift(new Range(10, 100), 3, true), new Range(13, 103));
        assertEquals("Singular, positive, true", Range.shift(new Range(-1, -1), 0.1, true), new Range(-0.9, -0.9));
        assertEquals("Negative, negative, false", Range.shift(new Range(-100, -10), -10, false), new Range(-110, -10));
        assertEquals("Spanning, negative, true", Range.shift(new Range(-10, 10), -5, true), new Range(-15, 5));
        assertEquals("Positive, negative, true", Range.shift(new Range(100, 101), -25, true), new Range(75, 76));
        assertEquals("Singular, negative, true", Range.shift(new Range(10, 10), -4, true), new Range(6, 6));
        assertEquals("Negative, partly crossing zero, false", Range.shift(new Range(-100, -50), 55, false), new Range(-45, 0));
        assertEquals("Spanning, partly crossing zero, true", Range.shift(new Range(-10, 10), 1, true), new Range(-9, 11));
        assertEquals("Positive, partly crossing zero, false", Range.shift(new Range(10, 20), -15, false), new Range(0, 5));
        assertEquals("Positive, partly crossing zero, true", Range.shift(new Range(10, 20), -15, true), new Range(-5, 5));
        assertEquals("Negative, completely crossing zero, false", new Range(0, 0), Range.shift(new Range(-20, -10), 40, false));
        assertEquals("Spanning, completely crossing zero, true", new Range(10, 30), Range.shift(new Range(-10, 10), 20, true));
        assertEquals("Positive, completely crossing zero, false", new Range(0, 0), Range.shift(new Range(10, 100), -300, false));
        assertEquals("Singular, completely crossing zero, true", new Range(-9, -9), Range.shift(new Range(1, 1), -10, true));
        }
}
