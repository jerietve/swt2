package org.jfree.data.test;

import junit.framework.TestCase;
import org.jfree.data.DataUtilities;
import org.jfree.data.*;

import org.jfree.data.KeyedValues;

public class DataUtilitiesTest extends TestCase {

	

	protected void setUp() throws Exception {
		super.setUp();
		//fill nulltable
		int i,j;
		for (i = 0; i< 5; i++)
		{
			for (j=0;j < 5; j++)
			{
				nullTable.addValue(null,i,j);
			}
		}
		
		//fill numbersAndNullTable
		for (i=0;i<5;i++)
		{
			for (j=0;j<5;j++)
			{
				if (i==j) numbersAndNullTable.addValue(null, i, j);
				else numbersAndNullTable.addValue(i+j,i,j);
			}
		}
		
		//fill numbersPosAndNegTable
		//	0	-1	2	-3	4
		//	-1	-1	2	-3	4
		//	2	2	2	-3	4
		//	-3	-3	-3	-3	4
		//	4	4	4	4	4
		numbersPosAndNegTable.addValue(0,0,0);
		numbersPosAndNegTable.addValue(-1,0,1);
		numbersPosAndNegTable.addValue(2,0,2);
		numbersPosAndNegTable.addValue(-3,0,3);
		numbersPosAndNegTable.addValue(4,0,4);
		
		numbersPosAndNegTable.addValue(-1,1,0);
		numbersPosAndNegTable.addValue(-1,1,1);
		numbersPosAndNegTable.addValue(2,1,2);
		numbersPosAndNegTable.addValue(-3,1,3);
		numbersPosAndNegTable.addValue(4,1,4);
		
		numbersPosAndNegTable.addValue(2,2,0);
		numbersPosAndNegTable.addValue(2,2,1);
		numbersPosAndNegTable.addValue(2,2,2);
		numbersPosAndNegTable.addValue(-2,2,3);
		numbersPosAndNegTable.addValue(4,2,4);
		
		numbersPosAndNegTable.addValue(-3,3,0);
		numbersPosAndNegTable.addValue(-3,3,1);
		numbersPosAndNegTable.addValue(-3,3,2);
		numbersPosAndNegTable.addValue(-3,3,3);
		numbersPosAndNegTable.addValue(4,3,4);
		
		numbersPosAndNegTable.addValue(4,4,0);
		numbersPosAndNegTable.addValue(4,4,1);
		numbersPosAndNegTable.addValue(4,4,2);
		numbersPosAndNegTable.addValue(4,4,3);
		numbersPosAndNegTable.addValue(4,4,4);

		//fill for createNumberArray
		for (i=0;i<5;i++)
		{
			testDoubleSingle[i] = (double)i;
			expectedNumberSingle[i] = i;
			
		}
		
		//fill for createNumberArray2D
		for (i=0;i<5;i++)
		{
			for (j=0;j<5;j++)
			{
				testDoubleMulti[i][j] = (double)(i+j);
				expectedNumberMulti[i][j] = i+j;
			}
		}
		
		//fill for test_getCumulativePercentages
		
		for (i=0;i<5;i++)
		{
			onlyNegativeKV.setValue(i, (Number)(i-5));
			onlyPositiveKV.setValue(i,(Number)i);
			posAndNegKV.setValue(i, (Number)(i-5));
			posAndNegKV.setValue(i+5, (Number)i);
			removedItemsKV.setValue(i, (Number)i);
		}
		
		removedItemsKV.removeValue(1);
		removedItemsKV.removeValue(2);
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void test_calculateColumnTotal()
	{
		//assertEquals(-1,DataUtilities.calculateColumnTotal(null, 0));
		assertEquals(0,DataUtilities.calculateColumnTotal(emptyTable, 0),0);
		assertEquals(0,DataUtilities.calculateColumnTotal(nullTable, 0),0);
		assertEquals(10,DataUtilities.calculateColumnTotal(numbersAndNullTable, 0),0);
		assertEquals(2,DataUtilities.calculateColumnTotal(numbersPosAndNegTable, 0),0);
		
	}
	
	public void test_calculateRowTotal()
	{
		//assertEquals(-1,DataUtilities.calculateRowTotal(null, 0));
		assertEquals(0,DataUtilities.calculateRowTotal(emptyTable, 0),0);
		assertEquals(0,DataUtilities.calculateRowTotal(nullTable, 0),0);
		assertEquals(16,DataUtilities.calculateRowTotal(numbersAndNullTable, 2),0);
		assertEquals(1,DataUtilities.calculateRowTotal(numbersPosAndNegTable, 1),0);
		
	}
	
	public void test_createNumberArray()
	{
		assertSame(expectedNumberSingle,DataUtilities.createNumberArray(testDoubleSingle));
		
	}
	
	public void test_createNumberArray2D()
	{
		
		assertSame(expectedNumberMulti,DataUtilities.createNumberArray2D(testDoubleMulti));
	}
	
	public void test_getCumulativePercentages()
	{
		

		KeyedValues k = (DataUtilities.getCumulativePercentages(removedItemsKV));
		int p;
		for (p=0;p<k.getItemCount();p++)
		{
			System.out.println(k.getValue(p)+" "+k.getKey(p));
		}
	}
	
	private DefaultKeyedValues2D emptyTable = new DefaultKeyedValues2D();
	
	private DefaultKeyedValues2D nullTable = new DefaultKeyedValues2D();
	
	private DefaultKeyedValues2D numbersAndNullTable = new DefaultKeyedValues2D();
	private DefaultKeyedValues2D numbersPosAndNegTable = new DefaultKeyedValues2D();
	//private DefaultKeyedValues2D overflowTable = new DefaultKeyedValues2D();
	
	private double[] testDoubleSingle = new double[5];
		private Number[] expectedNumberSingle = new Number[5];
		
	private double[][] testDoubleMulti = new double[5][5];
		private Number[][] expectedNumberMulti = new Number[5][5];
		
	private DefaultKeyedValues onlyNegativeKV = new DefaultKeyedValues();
	private DefaultKeyedValues posAndNegKV = new DefaultKeyedValues();
	private DefaultKeyedValues onlyPositiveKV = new DefaultKeyedValues();
	private DefaultKeyedValues removedItemsKV = new DefaultKeyedValues();
		

}
