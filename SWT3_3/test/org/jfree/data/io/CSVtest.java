package org.jfree.data.io;

import org.jfree.data.category.CategoryDataset;
import org.jfree.data.io.CSV;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Random;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CSVtest {
	
	private CSV testCSV;		// , and " delimiters
	private CSV testCSV2;		// \t and ' delimters

	// Reader that use , and " delimiters
	private Reader readerEmpty;
	private Reader readerUnicode;
	private Reader readerRandomNonText;
	private Reader readerSimpleFile;
	private Reader readerEmptyCells;
	private Reader readerRowsDiffLength;
	
	// Readers below use tab and ' delimiters
	private Reader readerUnicode2;
	private Reader readerSimpleFile2;
	private Reader readerEmptyCells2;
	private Reader readerRowsDiffLength2;
	
	// Test strings used as input for readers
	private String stringEmpty;
	private String stringUnicode;
	private String stringRandomNonText;
	private String stringSimpleFile;
	private String stringEmptyCells;
	private String stringRowsDiffLength;
	
	@Before
	public void setUp() throws Exception {
		testCSV = new CSV();
		testCSV2 = new CSV('\t', '\'');
		
		// Empty reader
		stringEmpty = "";
		readerEmpty = new StringReader(stringEmpty);
		
		// Unicode readers
		stringUnicode = "";
		int i;
		Random rand; //random unicode.
		for (i = 0; i < 1000; i++)
		{
			rand = new Random();
			stringUnicode += Integer.toHexString(rand.nextInt(0x1000) + 0x1000);
		}
		readerUnicode = new StringReader(stringUnicode);
		readerUnicode2 = new StringReader(stringUnicode.replace(",", "\t").replace("\"", "\'"));
		
		// Random non-text reader
		stringRandomNonText = "#*(($*#@*(*$*#(@*(()))Z)Z)Z))Z)Z)JDJD__________+@+@++++=#*##*$*$";
		
		readerRandomNonText = new StringReader(stringRandomNonText);
		
		// Simple file reader
		stringSimpleFile = "";
		for (i=0;i<500;i++)
		{
			rand = new Random();
			if (i % 5 == 0 && i % 25 != 0) stringSimpleFile += "\",\"";
			else if (i == 499) stringSimpleFile += "\"";
			else if (i == 0) stringSimpleFile += "\"";
			else if (i % 25 == 0) stringSimpleFile += "\"\n\"";
			else stringSimpleFile += i;
		}
		
		readerSimpleFile = new StringReader(stringSimpleFile);
		readerSimpleFile2 = new StringReader(stringSimpleFile.replace(",", "\t").replace("\"", "\'"));
		
		// Empty cells reader
		stringEmptyCells = "";
		
		for(int j = 0; j < 50; j++) {
			for(i = 0; i < 20; i++) {
				stringEmptyCells += ",";
			}
			stringEmptyCells += "\n";
		}
		
		readerEmptyCells = new StringReader(stringEmptyCells);
		readerEmptyCells2 = new StringReader(stringEmptyCells.replace(",", "\t").replace("\"", "\'"));
		
		// Different rows reader
		stringRowsDiffLength = "";
		
		for(int j = 0; j < 50; j++) {
			for(i = 0; i < 20 + j; i++) {
				stringRowsDiffLength += "\"eroiR\",";
			}
			stringRowsDiffLength += "\"lala\"\n";
		}
		
		readerRowsDiffLength = new StringReader(stringRowsDiffLength);
		readerRowsDiffLength2 = new StringReader(stringRowsDiffLength.replace(",", "\t").replace("\"", "\'"));
		
		System.out.println(stringSimpleFile);
	}

	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testDefaultConstructor() {
		// Test default constructor
		try {
			new CSV();
		} catch(Exception e) {
			fail("Exception happened while constructing CSV object");
		}
	}
	
	@Test
	public void testSpecialDelimitersConstructor() {
		// Test special delimiters constructor
		try {
			new CSV('\t', '\'');
		} catch(Exception e) {
			fail("Exception happened while constructing CSV object");
		}
	}
	
	@Test
	public void testEmptyCSV() {
		CategoryDataset cds;
		
		cds	= readShouldPass(testCSV, readerEmpty);
		
		// Test if columns and rows are read properly
		assertEquals(0, cds.getColumnCount());
		assertEquals(0, cds.getRowCount());
		
		// Test other delimiters (tab and ') (not random non-text because that doesn't make sense)		
		cds	= readShouldPass(testCSV2, readerEmpty);
		assertEquals(0, cds.getColumnCount());
		assertEquals(0, cds.getRowCount());
	}
	
	@Test
	public void testUnicodeCSV() {
		CategoryDataset cds;
		
		cds = readShouldPass(new CSV(), readerUnicode);
		
		assertEquals(1, cds.getColumnCount());
		assertEquals(1, cds.getRowCount());
		
		cds	= readShouldPass(testCSV2, readerUnicode2);
		
		assertEquals(1, cds.getColumnCount());
		assertEquals(1, cds.getRowCount());
	}
	
	@Test
	public void testRandomCSV() {
		CategoryDataset cds;
		
		cds = readShouldPass(testCSV, readerRandomNonText);
		
		assertEquals(1, cds.getColumnCount());
		assertEquals(1, cds.getRowCount());
		
		// Not necessary to also test other delimiters, because it does not have delimiters
	}
	
	@Test
	public void testSimpleCSV() {
		CategoryDataset cds;
		
		cds = readShouldPass(testCSV, readerSimpleFile);
		
		assertEquals(5, cds.getColumnCount());
		assertEquals(100, cds.getRowCount());
		
		// Check some data fields
		assertTrue(cds.getValue(0, 0) == new Integer(1234));
		assertTrue(cds.getValue(1, 0) == new Integer(6789));
		assertTrue(cds.getValue(1, 1) == new Integer(26272829));
		assertTrue(cds.getValue(2, 2) == new Integer(56575859));
		
		cds	= readShouldPass(testCSV2, readerSimpleFile2);
		
		assertEquals(5, cds.getColumnCount());
		assertEquals(100, cds.getRowCount());
		
		// Check some data fields
		assertTrue(cds.getValue(0, 0) == new Integer(1234));
		assertTrue(cds.getValue(1, 0) == new Integer(6789));
		assertTrue(cds.getValue(1, 1) == new Integer(26272829));
		assertTrue(cds.getValue(2, 2) == new Integer(56575859));
	}
	
	@Test
	public void testEmptyCellsCSV() {
		CategoryDataset cds;
		
		cds = readShouldPass(testCSV, readerEmptyCells);
		
		assertEquals(20, cds.getColumnCount());
		assertEquals(50, cds.getRowCount());
		
		cds	= readShouldPass(testCSV2, readerEmptyCells2);
		
		assertEquals(20, cds.getColumnCount());
		assertEquals(50, cds.getRowCount());
	}
	
	@Test
	public void testDiffRowsCSV() {
		CategoryDataset cds;
		
		cds = readShouldPass(testCSV, readerRowsDiffLength);
		
		//assertEquals(20, cds.getColumnCount());	// Should this be 20 columns?
		assertEquals(50, cds.getRowCount());
		
		cds	= readShouldPass(testCSV2, readerRowsDiffLength2);
		
		//assertEquals(20, cds.getColumnCount());
		assertEquals(50, cds.getRowCount());
	}
	
	
	// PRIVATE METHODS BELOW, NO MORE TESTS
	
	private CategoryDataset readShouldPass(CSV testCSV, Reader reader) {
		CategoryDataset cds = null;
		
		try {
			cds = testCSV.readCategoryDataset(readerEmpty);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Exception happened");
		}
		
		return cds;
	}
	
	/*private void readShouldFail(CSV testCSV, Reader reader) {
		try {
			testCSV.readCategoryDataset(readerEmpty);
			fail("Exception expected, but did not happen");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
