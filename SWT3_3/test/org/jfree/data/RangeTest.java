package org.jfree.data;

import org.jfree.data.Range;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RangeTest
{
    // Input bounds for testing purposes (just integers, no Range object)
    private double[] negativeBounds, zeroSpanningBounds, positiveBounds, sameBounds;

    // Ranges based on bounds above
    private Range negativeRange, zeroSpanningRange, positiveRange, sameRange;


    @Before
    public void setUp() throws Exception
    {
        // Initialize bounds by taking a random range for each equivalence partition
        negativeBounds = new double[] { -3, -1 };
        zeroSpanningBounds = new double[] { -7, 5 };
        positiveBounds = new double[] { 0, 5789032.27 };
        sameBounds = new double[] { Math.PI, Math.PI };

        // Initialize Range objects using the bounds above
        negativeRange = new Range(negativeBounds[0], negativeBounds[1]);
        zeroSpanningRange = new Range(zeroSpanningBounds[0], zeroSpanningBounds[1]);
        positiveRange = new Range(positiveBounds[0], positiveBounds[1]);
        sameRange = new Range(sameBounds[0], sameBounds[1]);

    }


    @After
    public void tearDown() throws Exception
    {
        negativeBounds = null;
        zeroSpanningBounds = null;
        positiveBounds = null;
        sameBounds = null;

        negativeRange = null;
        zeroSpanningRange = null;
        positiveRange = null;
        sameRange = null;
    }


    @Test
    public void testContains()
    {
        double[][] testRanges = { negativeBounds, zeroSpanningBounds, positiveBounds, sameBounds };

        // Test four type of ranges
        for(int i = 0; i < 4; i++)
        {
            double lowerBound = testRanges[i][0];
            double upperBound = testRanges[i][1];
            Range range = new Range(lowerBound, upperBound);

            assertFalse("Just below the lower bound", range.contains(lowerBound - 1));
            assertFalse("Just above the upper bound", range.contains(upperBound + 1));

            // It is assumed that the lower and upper bounds are included in the range
            assertTrue("The lower bound", range.contains(lowerBound));
            assertTrue("The upper bound", range.contains(upperBound));

            // Do not test the following boundary values, for a range that contains only one value
            if(lowerBound != upperBound)
            {
                assertTrue("Just above the lower bound", range.contains(lowerBound + 1));
                assertTrue("Somewhere in the middle", range.contains((lowerBound + upperBound) / 2));
                assertTrue("Just below the upper bound", range.contains(upperBound - 1));
            }
        }
    }


    @Test
    public void testEqualsDifferentRange()
    {
        assertFalse("Positive range taking negative range", positiveRange.equals(negativeRange));
    }
    
    @Test
    public void testEqualsDifferentUpperBoundSign() 
    {
        Range zeroSpanningDifferentSign = new Range(zeroSpanningBounds[0], -zeroSpanningBounds[1]);
        assertFalse("Range spanning zero taking same range, but with different upper bound sign", positiveRange.equals(zeroSpanningDifferentSign));
    }
    
    @Test
    public void testEqualsNotARange() 
    {
        class NotARealRange
        {
            public double upper, lower;


            public NotARealRange(double lower, double upper)
            {
                this.upper = upper;
                this.lower = lower;
            }
        }

        assertFalse("Negative range and other class instance", new Range(-3, -2).equals(new NotARealRange(-3, -2)));
    }
    
    @Test
    public void testEqualsNull() 
    {
        assertFalse("Positive range, input null", positiveRange.equals(null));
    }
    
    @Test
    public void testEqualsDifferentUpperBound() 
    {
        Range sameRangeLowerbound = new Range(sameBounds[0], sameBounds[0] + 1);
        assertFalse("Range with one value taking range with this value as lower bound, upperbound greater", sameRange.equals(sameRangeLowerbound));
    }
    
    @Test
    public void testEqualsShiftedOneRange() 
    {
        Range otherPositiveRange = new Range(positiveBounds[0] + 1, positiveBounds[1] + 1);
        assertFalse("Positive range taking another range with the same length", sameRange.equals(otherPositiveRange));
    }
    
    @Test
    public void testEqualsSlightlyShiftedRange() 
    {
        Range slightlyShiftedNegative = new Range(negativeBounds[0] + Double.MIN_VALUE, negativeBounds[1] + Double.MIN_VALUE);
        assertFalse("Negative range taking copy that is slightly shifted towards positive infinity", negativeRange.equals(slightlyShiftedNegative));
    }


    @Test
    public void testLengthNegativeBounds()
    {
        assertEquals("Completly negative bounds", negativeBounds[1] - negativeBounds[0], negativeRange.getLength(), 0.000001d);
    }


    @Test
    public void testLengthZeroSpanning()
    {
        assertEquals("Zero spanning bounds", zeroSpanningBounds[1] - zeroSpanningBounds[0], zeroSpanningRange.getLength(), 0.000001d);
    }


    @Test
    public void testLengthPositiveBounds()
    {
        assertEquals("Completly positive bounds", positiveBounds[1] - positiveBounds[0], positiveRange.getLength(), 0.000001d);
    }


    @Test
    public void testLengthEqualBounds()
    {
        assertEquals("Same bounds", sameBounds[1] - sameBounds[0], sameRange.getLength(), 0.000001d);
    }
    
    
    @Test
    public void testExpandToIncludeNullRange()
    {
        assertEquals(new Range(30, 30), Range.expandToInclude(null, 30));
    }
    
    
    @Test
    public void testExpandToIncludeLowerValue()
    {
        assertEquals(new Range(10, 100), Range.expandToInclude(new Range(50, 100), 10));
    }
    
    
    @Test
    public void testExpandToIncludeHigherValue()
    {
        assertEquals(new Range(-200, 30), Range.expandToInclude(new Range(-200, 0), 30));
    }
    
    
    @Test
    public void testExpandToIncludeContainingValue()
    {
        assertEquals(new Range(1, 2), Range.expandToInclude(new Range(1, 2), 1.5));
    }
    
    
    @Test(expected = IllegalArgumentException.class)
    public void testExpandNullRange()
    {
        Range.expand(null, 3, 4);
    }
    
    
    @Test
    public void testExpand()
    {
        assertEquals(new Range(-10, 120), Range.expand(new Range(0, 120), 0.1, 0.2));
    }
    
    
    @Test
    public void testConstrainBelow()
    {
        assertEquals(10, new Range(10,  100).constrain(-30), 0);
    }
    
    
    @Test
    public void testConstrainIn()
    {
        assertEquals(50, new Range(10,  100).constrain(50), 0);
    }
    
    
    @Test
    public void testConstrainAbove()
    {
        assertEquals(100, new Range(10,  100).constrain(10030), 0);
    }
    
    
    @Test(expected = IllegalArgumentException.class)
    public void testConstructor()
    {
        new Range(100, 10);
    }
    
    
    @Test
    public void testHashCode()
    {
        assertNotEquals(new Range(0, 0).hashCode(), new Range(1, 1).hashCode());
    }
}
