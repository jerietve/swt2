package org.jfree.data;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.jfree.data.Range;

@RunWith(Parameterized.class)
public class RangeTestCombineIntersects
{
    private String message;
    private Range range1, range2, combination;
    private Boolean intersecting;
    
    
        
    public RangeTestCombineIntersects(String message, Range range1, Range range2, Range combination, Boolean intersecting)
    {
        this.message = message;
        this.range1 = range1;
        this.range2 = range2;
        this.combination = combination;
        this.intersecting = intersecting;
    }
    
    
    @Parameters(name="{index}_{0}")
    public static Collection inputs()
    {
        return Arrays.asList(new Object[][]
        {
            {"Negative/null", new Range(-100,  -10), null, new Range(-100, -10), false},
            {"Null/singular", null, new Range(10, 10), new Range(10, 10), false},
            {"Null/null", null, null, null, false},
            {"Negative/positive, r1ub < r2lb", new Range(-100,  -10), new Range(10, 100), new Range(-100, 100), false},
            {"Singular/positive, r1ub == r2lb", new Range(-3,  -3), new Range(-3, -1), new Range(-3, -1), true},
            {"Spanning/positive, r1lb < r2lb, r1ub > r2lb, r1ub < r2ub", new Range(-10, 100), new Range(90, 1000), new Range(-10, 1000), true},
            {"Singular/spanning, r1lb < r2lb, r1ub == r2ub", new Range(3.72, 3.72), new Range(-10E18, 3.72), new Range(-10E18, 3.72), true},
            {"Spanning/negative, r1lb < r2lb, r1ub > r2ub", new Range(-10E17, 10E17), new Range(-10E16, -10E15), new Range(-10E17, -10E15), true},
            {"Positive/positive, r1lb == r2lb, r1ub < r2ub", new Range(Math.E, Math.PI), new Range(Math.E, 10.3), new Range(Math.E, 10.3), true},
            {"Singular/singular, r1lb == r2lb, r1ub == r2ub", new Range(0, 0), new Range(0.0, 0.0), new Range(0, 0), true},
            {"Spanning/Spanning, r1lb == r2lb, r1ub > r2ub", new Range(-0.1, 0.1), new Range(-0.1, 0.0001), new Range(-0.1, 0.0001), true},
            {"Negative/negative, r1lb > r2lb, r1ub < r2ub", new Range(-10, -2), new Range(-3000, -0.0001), new Range(-3000, -0.0001), true},
            {"Spanning/spanning, r1lb > r2lb, r1ub == r2ub", new Range(-10, 2), new Range(-3, 2), new Range(-10, 2), true},
            {"Negative/negative, r1lb > r2lb, r1lb < r2ub, r1ub > r2ub", new Range(-10, -2), new Range(-3, -1), new Range(-10, -1), true},
            {"Singular/positive, r1lb == r2ub", new Range(3, 3), new Range(3, 10), new Range(3, 10), true},
            {"Positive/positive, r1lb > r2ub", new Range(3, 4), new Range(10, 100), new Range(3, 100), false}
        });
    }



    @Test
    public void testCombine()
    {
        assertEquals(message, combination, Range.combine(range1, range2));
    }
    
    
    @Test
    public void testIntersects()
    {
        if(range1 != null && range2 != null)
        {
            assertEquals(message, intersecting, range1.intersects(range2.getLowerBound(), range2.getUpperBound()));
        }
    }
}
