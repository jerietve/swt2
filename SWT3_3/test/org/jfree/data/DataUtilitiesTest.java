package org.jfree.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


@RunWith(JUnit4.class)
public class DataUtilitiesTest
{
    @Before
    public void setUp() throws Exception
    {
        // fill nulltable
        int i, j;
        for(i = 0; i < 5; i++)
        {
            for(j = 0; j < 5; j++)
            {
                nullTable.addValue(null, i, j);
            }
        }
        
        // Fill one row table
        for(i = 0; i < 5; i++) {
        	oneRowTable.addValue(100, 1, i);
        }

        // fill numbersAndNullTable
        for(i = 0; i < 5; i++)
        {
            for(j = 0; j < 5; j++)
            {
                if(i == j) numbersAndNullTable.addValue(null, i, j);
                else numbersAndNullTable.addValue(i + j, i, j);
            }
        }

		//fill numbersPosAndNegTable
		//	0	-1	2	-3	4
		//	-1	-1	2	-3	4
		//	2	2	2	-3	4
		//	-3	-3	-3	-3	4
		//	4	4	4	4	4
		numbersPosAndNegTable.addValue(0,0,0);
		numbersPosAndNegTable.addValue(-1,0,1);
		numbersPosAndNegTable.addValue(2,0,2);
		numbersPosAndNegTable.addValue(-3,0,3);
		numbersPosAndNegTable.addValue(4,0,4);
		
		numbersPosAndNegTable.addValue(-1,1,0);
		numbersPosAndNegTable.addValue(-1,1,1);
		numbersPosAndNegTable.addValue(2,1,2);
		numbersPosAndNegTable.addValue(-3,1,3);
		numbersPosAndNegTable.addValue(4,1,4);
		
		numbersPosAndNegTable.addValue(2,2,0);
		numbersPosAndNegTable.addValue(2,2,1);
		numbersPosAndNegTable.addValue(2,2,2);
		numbersPosAndNegTable.addValue(-2,2,3);
		numbersPosAndNegTable.addValue(4,2,4);
		
		numbersPosAndNegTable.addValue(-3,3,0);
		numbersPosAndNegTable.addValue(-3,3,1);
		numbersPosAndNegTable.addValue(-3,3,2);
		numbersPosAndNegTable.addValue(-3,3,3);
		numbersPosAndNegTable.addValue(4,3,4);
		
		numbersPosAndNegTable.addValue(4,4,0);
		numbersPosAndNegTable.addValue(4,4,1);
		numbersPosAndNegTable.addValue(4,4,2);
		numbersPosAndNegTable.addValue(4,4,3);
		numbersPosAndNegTable.addValue(4,4,4);
        
        // TODO: initialize overflow table
    
		//fill for createNumberArray
		for (i=0;i<5;i++)
		{
			testDoubleSingle[i] = (double)i;
			expectedNumberSingle[i] = i;
			
		}
		
		//fill for createNumberArray2D
		for (i=0;i<5;i++)
		{
			for (j=0;j<5;j++)
			{
				testDoubleMulti[i][j] = (double)(i+j);
				expectedNumberMulti[i][j] = i+j;
			}
		}
		
		//fill for test_getCumulativePercentages
		
		for (i=0;i<5;i++)
		{
			onlyNegativeKV.setValue(i, (Number)(i-5));
			onlyPositiveKV.setValue(i,(Number)i);
			posAndNegKV.setValue(i, (Number)(i-5));
			posAndNegKV.setValue(i+5, (Number)i);
			removedItemsKV.setValue(i, (Number)i);
		}
		
		removedItemsKV.removeValue(1);
		removedItemsKV.removeValue(2);
    }

    @After
    public void tearDown() throws Exception
    {
    }


    //--------------------------------------------
    // Calculate column total test methods
    //--------------------------------------------
    
    @Test
    public void testCalculateColumnTotalEmptyTable()
    {
        // assertEquals(-1,DataUtilities.calculateColumnTotal(null, 0.00001d));
        assertEquals(0, DataUtilities.calculateColumnTotal(emptyTable, 0), 0.00001d);
    }
    
    @Test
    // Added this test case to cover the one-time iteration of the for-loop in calculateColumnTotal()
    public void testCalculateColumnTotalOneRowTable() {
    	assertEquals(500, DataUtilities.calculateColumnTotal(oneRowTable, 0), 0.00001d);
    }


    @Test
    public void testCalculateColumnTotalNullTable()
    {
    	Double d = null;
        assertEquals(d, DataUtilities.calculateColumnTotal(nullTable, 0), 0.00001d);
    }


    @Test
    public void testCalculateColumnTotalNumbersAndNullTable()
    {
        assertEquals(10, DataUtilities.calculateColumnTotal(numbersAndNullTable, 0), 0.00001d);
    }


    @Test
    public void testCalculateColumnTotalPosAndNegTable()
    {
        assertEquals(4, DataUtilities.calculateColumnTotal(numbersPosAndNegTable, 0), 0.00001d);
    }
    
    @Test
    public void testCalculateColumnTotalOverflowTable()
    {
        assertEquals(4, DataUtilities.calculateColumnTotal(overflowTable, 0), 0.00001d);
    }

    //--------------------------------------------
    // Calculate row total test methods
    //--------------------------------------------
    
    @Test
    public void test_calculateRowTotalEmptyTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(emptyTable, 0), 0.00001d);
    }
    
    @Test
    public void test_calculateRowTotalNullTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(nullTable, 0), 0.00001d);
    }
    
    @Test
    // Added this test case to cover the one-time iteration of the for-loop in calculateRowTotal()
    public void test_calculateRowTotalOneColumnTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(oneColumnTable, 0), 0.00001d);
    }
    
    @Test
    public void test_calculateRowTotalNumbersAndNullTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(emptyTable, 0), 0.00001d);
    }
    
    @Test
    public void test_calculateRowTotalNumbersPosAndNegTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(numbersPosAndNegTable, 0), 0.00001d);
    }
    
    @Test
    public void test_calculateRowTotalOverflowTable()
    {
        assertEquals(0, DataUtilities.calculateRowTotal(overflowTable, 0), 0.00001d);
    }
    
    @Test
	public void test_createNumberArray()
	{
		assertSame(expectedNumberSingle,DataUtilities.createNumberArray(testDoubleSingle));
	}
	
    @Test
    // Added test case to cover null data array condition
	public void test_createNumberArrayNull()
	{
    	try {
    		DataUtilities.createNumberArray(null);
    		fail("Exceptioned expected, but not thrown");
    	} catch(IllegalArgumentException e) {
    		
    	}
	}
    
    @Test
	public void test_createNumberArray2D()
	{
		assertSame(expectedNumberMulti,DataUtilities.createNumberArray2D(testDoubleMulti));
	}
    
    @Test
    // Added test case to cover null data array condition
	public void test_createNumberArray2DNull()
	{
    	try {
    		DataUtilities.createNumberArray2D(null);
    		fail("Exceptioned expected, but not thrown");
    	} catch(IllegalArgumentException e) {
    		
    	}
	}
	
    @Test
	public void test_getCumulativePercentages()
	{
		KeyedValues k = (DataUtilities.getCumulativePercentages(removedItemsKV));
		int p;
		for (p=0;p<k.getItemCount();p++)
		{
			System.out.println(k.getValue(p)+" "+k.getKey(p));
		}
	}

    // private static DataUtilities da;
    private DefaultKeyedValues2D emptyTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D nullTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D oneRowTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D oneColumnTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D numbersAndNullTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D numbersPosAndNegTable = new DefaultKeyedValues2D();
    private DefaultKeyedValues2D overflowTable = new DefaultKeyedValues2D();

	private double[] testDoubleSingle = new double[5];
	private Number[] expectedNumberSingle = new Number[5];
	
	private double[][] testDoubleMulti = new double[5][5];
	private Number[][] expectedNumberMulti = new Number[5][5];
	
	private DefaultKeyedValues onlyNegativeKV = new DefaultKeyedValues();
	private DefaultKeyedValues posAndNegKV = new DefaultKeyedValues();
	private DefaultKeyedValues onlyPositiveKV = new DefaultKeyedValues();
	private DefaultKeyedValues removedItemsKV = new DefaultKeyedValues();
}
