import junit.framework.TestCase;

public class BubbleSortTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
		//inputArray = new Array ( 1,3,4,5,2};
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testSimple()
	{
		int t[] = BubbleSort.sort(simpleArray);
		for (int p = 0 ; p< t.length; p++) 
			assertSame(expectedSimpleArray[p], BubbleSort.sort(simpleArray)[p]);
	}
	public void testEmpty()
	{
		int t[] = BubbleSort.sort(emptyArray);
		for (int p = 0 ; p< t.length; p++) 
			assertSame(emptyArray[p], BubbleSort.sort(emptyArray)[p]);
	}
	public void testSorted()
	{
		int t[] = BubbleSort.sort(sortedArray);
		for (int p = 0 ; p< t.length; p++) 
			assertSame(sortedArray[p], BubbleSort.sort(sortedArray)[p]);
	}
	public void testOne()
	{
		int t[] = BubbleSort.sort(oneArray);
		for (int p = 0 ; p< t.length; p++) 
			assertSame(oneArray[p], BubbleSort.sort(oneArray)[p]);
	}
	public void testTen()
	{
		int t[] = BubbleSort.sort(tenArray);
		for (int p = 0 ; p< t.length; p++) 
			assertSame(expectedTenArray[p], BubbleSort.sort(tenArray)[p]);
	}

	private int[] emptyArray =  {} ;	
	private int[] sortedArray = {1,2,3,4,5};
	private int[] oneArray = {1};
	private int[] tenArray = {5,7,6,1,3,2,4,9,0,8};
	private int[] expectedTenArray = {0,1,2,3,4,5,6,7,8,9};
	
	private int[] simpleArray = { 1,5,2,4,3 };
	private int[] expectedSimpleArray = { 1,2,3,4,5 };
}
