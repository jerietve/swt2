public class BubbleSort {
		public static int[] sort(int[] x) {
			int i ;
			boolean domore = true;
			int temp;
			while (domore) {
				domore = false;
				for (i=0; i < x.length - 1; i++ ) {
					if (x[i] > x[i+1]) {
						temp = x[i];
						x[i] = x[i+1];
						x[i+1] = temp;
						domore = true;
					}
				}
			}
			return (x);
	}
}