require "watir-webdriver"

##############
# Input data #
##############
$verbose_mode = true
user1_name = 'User1-swt4'
user1_email = 'user1.swt4@gmail.com'
user1_password = 'softwaretesting'
user2_name = 'User2-swt4'
user2_email = 'user2.swt4@gmail.com'
user2_password = 'softwaretesting'
repository_name = 'test_repo'

###########
# Classes #
###########
class User
	attr_accessor :name, :email, :password
	
	def initialize(name, email, password)
		@name = name
		@email = email
		@password = password
	end
end


class Issue
	attr_accessor :title, :description, :assignee, :id, :type, :resolved

	def initialize(title, options = {})
		@title = title
		@description = options[:description]
		@assignee = options[:assignee]
		@type = options[:type]
		@resolved = options[:resolved] || false
	end
end
	
	

#############
# Functions #
#############
def sign_in_bitbucket(browser, user)
	log "Signing in user: #{user.name}"
	browser.goto 'bitbucket.org'
	if browser.link(title: user.name).exists?
		# User is already logged in
		return
	elsif browser.link(id: 'user-dropdown-trigger').exists?
		# Another user is logged in, sign out first
		sign_out_bitbucket(browser)
	end
	browser.link(text: /log in/i).click
	browser.text_field(id: 'id_username').set user.email
	browser.text_field(id: 'id_password').set user.password
	browser.form(id: 'login-form').submit
end


def sign_out_bitbucket(browser)
	log 'Signing out'
	browser.goto 'bitbucket.org'
	if not browser.link(id: 'user-dropdown-trigger').exists? then return end
	browser.link(id: 'user-dropdown-trigger').click
	browser.link(text: 'Log out').click
end

# Creates a new issue in an empty issue tracker. It assigns the ID created by the issue tracker to the issue.
# * *Pre*:
#	- Browser is on the issue tracker page
def create_issue(browser, issue)
	log "Creating issue: #{issue.title}"
	if browser.link(text: /first issue/).exists?
		browser.link(text: /first issue/).click
	else
		browser.link(text: /create issue/i).click
	end
	browser.text_field(id: 'id_title').set issue.title
	if issue.description then browser.text_field(id: 'id_content').set issue.description end
	if issue.assignee
		browser.span(text: /select a user/i).click
		browser.li(text: /#{Regexp.escape(issue.assignee.name)}/).click
	end
	if issue.type
		browser.span(text: 'bug').click
		browser.li(text: issue.type).click
	end
	browser.form(id: 'issue-form').submit
	# Save the issue id
	issue.id = browser.span(class: 'issue-id').text.scan(/\d+/).first
	log "Issue got ID \##{issue.id}"
end


# Deletes the given issue from the tracker, if it exists
# * *Pre*:
#	- Browser is on issue tracker page
def delete_issue(browser, issue)
	log "Deleting issue \##{issue.id}"
	if browser.link(title: /\##{Regexp.escape(issue.id)}/).exists?
		browser.link(title: /\##{Regexp.escape(issue.id)}/).click
		browser.link(text: /more/i).click
		browser.link(text: /delete/i).click
		browser.alert.ok
	else
		raise "Cant't delete issue \##{issue.id}: doesn't exist"
	end
end


# Makes sure the given issue is present, checking all available attributes. Currently supported attributes: title,
# description, assignee, type, ID, resolved. Raises exception in case verification fails.
#
# * *Pre*:
#	- Browser is on issues page
#	- Issue has an ID
def verify_issue(browser, issue)
	log "Verifying issue \##{issue.id}"
	if browser.link(title: /\##{Regexp.escape(issue.id)}/).exists?
		browser.link(title: /\##{Regexp.escape(issue.id)}/).click
		if not browser.h1(id: 'issue-title').text == issue.title
			raise "Issue \##{issue.id} does not have title #{issue.title}"
		elsif issue.type and not browser.dd(class: 'type with-icon').link(text: /#{issue.type}/).exists?
			raise "Issue \##{issue.id} does not have type #{issue.type}"
		elsif issue.description and not browser.div(class: 'issue-description').p(text: /#{issue.description}/).exists?
			raise "Issue \##{issue.id} does not have description #{issue.description}"
		elsif issue.assignee and not browser.dd(class: /assignee/).link(href: /#{issue.assignee.name}/).exists?
			raise "Issue \##{issue.id} does not have assignee #{issue.assignee.name}"
		else
			status = browser.dd(class: /status/).link.text
			if issue.resolved
				raise "Issue \##{issue.id} does not have status resolved" unless status == 'resolved'
			else
				if status != 'open' and status != 'new'
					raise "Issue \##{issue.id} does not have status open or new"
				end
			end
		end
	else
		raise "Issue \##{issue.id} is not in the issue tracker"
	end
end


# Changes the title of the given issue to the new title.
#
# * *Pre*:
#	- Browser is on issues page
#	- Issue has an ID
def change_issue_title(browser, issue, new_title)
	log "Changing title of issue \##{issue.id} to #{new_title}"
	if browser.link(title: /\##{Regexp.escape(issue.id)}/).exists?
		browser.link(title: /\##{Regexp.escape(issue.id)}/).click
		browser.link(id: 'edit-issue').click
		browser.text_field(id: 'id_title').set new_title
		browser.form(id: 'issue-form').submit
		issue.title = new_title
	else
		raise "Issue \##{issue.id} is not in the issue tracker"
	end
end


# Changes the status of the given issue to the resolved.
#
# * *Pre*:
#	- Browser is on issues page
#	- Issue has an ID
#	- Issue hasn't been resolved yet
def resolve_issue(browser, issue)
	log "Resolving issue \##{issue.id}"
	if browser.link(title: /\##{Regexp.escape(issue.id)}/).exists?
		browser.link(title: /\##{Regexp.escape(issue.id)}/).click
		browser.link(id: 'workflow').click
		browser.link(text: 'resolved').click
		browser.button(text: 'Change').click
		issue.resolved = true
	else
		raise "Issue \##{issue.id} is not in the issue tracker"
	end
end


# * *Pre*:
#	- User is signed in
def go_to_issue_page(browser, repository_name)
	log 'Going to issues page'
	if browser.link(text: "#{repository_name}").exists?
		# Already in the right repository
		browser.link(text: /issues/i).click
	else
		browser.link(text: /repositories/i).click
		browser.link(text: /#{repository_name}/).click
		browser.link(text: /issues/i).click
	end
end


# Filters the issues by type
# * *Pre*:
#	- The browser is on the issues page
#	- There is an issue of the given type on the page
def filter_by_type(browser, type)
	log "Filtering by type: #{type}"
	browser.link(class: "icon #{type}", text: "#{type}").click
end


# Makes sure all issues are shown
# * *Pre*:
#	- The browser is on the issues page
def filter_all(browser)
	log 'Showing all issues'
	browser.li(id: 'all').link.click
end

# Bla it's getting tired.. I mean late
def go_to_inbox(browser)
	log 'Opening inbox'
	browser.link(id: 'user-dropdown-trigger').click
	browser.link(text: /inbox/i).click
end


# Bla it's getting tired.. I mean late
def verify_notification(browser, issue)
	log "Verifying the existence of a message in the inbox notifying about issue \##{issue.id}"
	if not browser.link(text: /Issue \##{issue.id}: #{issue.title}/).exists?
		raise "No notification found about new issue \##{issue.id}"
	end
end


# Bla it's getting tired.. I mean late
def delete_all_notifications(browser)
	log 'Deleting all notifications'
	browser.header(id: 'notification-header').checkbox.set
	browser.button(text: /Delete/).click
end


def log(message)
	if $verbose_mode then puts message end
end


def log_heading(message)
	if $verbose_mode
		puts
		puts '-' * message.length
		puts message
		puts '-' * message.length
	end
end


#############
# Main code #
#############
failures = Array.new
num_tests = 0


log_heading 'Scenario: interaction test'
scenario_name = 'Interaction test'
num_tests += 1
begin
	log 'Setup'
	browser1 = Watir::Browser.new :ff
	browser2 = Watir::Browser.new :ff
	user1 = User.new(user1_name, user1_email, user1_password)
	user2 = User.new(user2_name, user2_email, user2_password)
	issue = Issue.new('test1', description: 'test1 text', assignee: user2)

	log 'Scenario start'
	sign_in_bitbucket(browser1, user1)
	go_to_issue_page(browser1, repository_name)
	create_issue(browser1, issue)
	sign_in_bitbucket(browser2, user2)
	go_to_issue_page(browser2, repository_name)
	verify_issue(browser2, issue)
	go_to_issue_page(browser2, repository_name)
	delete_issue(browser2, issue)

	log 'Cleanup'
	sign_out_bitbucket(browser1)
	browser1.close
	sign_out_bitbucket(browser2)
	browser2.close
rescue => e
	print 'F'
	failures.push("#{scenario_name} failed: #{e.message}\n#{e.backtrace}")
else
	print '.' unless $verbose_mode
end


log_heading 'Scenario: filter test'
scenario_name = 'Filter test'
num_tests += 1
begin
	log 'Setup'
	browser = Watir::Browser.new :ff
	user = User.new(user1_name, user1_email, user1_password)
	issue1 = Issue.new('test1', description: 'test1 text', assignee: user, type: 'bug')
	issue2 = Issue.new('test2', assignee: user, type: 'bug')
	issue3 = Issue.new('test3', type: 'proposal', description: 'test3 text')
	log 'Done initializing'

	log 'Scenario start'
	sign_in_bitbucket(browser, user)
	go_to_issue_page(browser, repository_name)
	create_issue(browser, issue1)
	create_issue(browser, issue2)
	create_issue(browser, issue3)
	go_to_issue_page(browser, repository_name)
	filter_by_type(browser, issue1.type)
	log 'Making sure test1 and test2 are visible, and test3 is not'
	if browser.link(title: /\##{Regexp.escape(issue1.id)}/).exists? and
			browser.link(title: /\##{Regexp.escape(issue2.id)}/).exists? and
			not browser.link(title: /\##{Regexp.escape(issue3.id)}/).exists?
		log 'All is fine'
	else
		raise 'Filtering did not work!'
	end

	log 'Cleanup'
	delete_issue(browser, issue1)
	delete_issue(browser, issue2)
	delete_issue(browser, issue3)
	sign_out_bitbucket(browser)
	browser.close	
rescue => e
	print 'F'
	failures.push("#{scenario_name} failed: #{e.message}\n#{e.backtrace}")
else
	print '.' unless $verbose_mode
end


log_heading 'Scenario: editing test'
scenario_name = 'Editing test'
num_tests += 1
begin
	log 'Setup'
	browser = Watir::Browser.new :ff
	user = User.new(user1_name, user1_email, user1_password)
	issue = Issue.new('test1', description: 'test1 text')
	log 'Done initializing'

	log 'Scenario start'
	sign_in_bitbucket(browser, user)
	go_to_issue_page(browser, repository_name)
	create_issue(browser, issue)
	go_to_issue_page(browser, repository_name)
	verify_issue(browser, issue)
	go_to_issue_page(browser, repository_name)
	change_issue_title(browser, issue, 'test2')
	go_to_issue_page(browser, repository_name)
	verify_issue(browser, issue)
	go_to_issue_page(browser, repository_name)
	resolve_issue(browser, issue)
	sleep(2)
	go_to_issue_page(browser, repository_name)
	filter_all(browser)
	verify_issue(browser, issue)

	log 'Cleanup'
	go_to_issue_page(browser, repository_name)
	filter_all(browser)
	delete_issue(browser, issue)
	sign_out_bitbucket(browser)
	browser.close
rescue => e
	print 'F'
	failures.push("#{scenario_name} failed: #{e.message}\n#{e.backtrace}")
else
	print '.' unless $verbose_mode
end


log_heading 'Scenario: notification test'
scenario_name = 'Notification test'
num_tests += 1
begin
	log 'Setup'
	browser = Watir::Browser.new :ff
	user1 = User.new(user1_name, user1_email, user1_password)
	user2 = User.new(user2_name, user2_email, user2_password)
	issue = Issue.new('test1', description: 'test1 text', assignee: user2)
	log 'Done initializing'

	log 'Scenario start'
	sign_in_bitbucket(browser, user1)
	go_to_issue_page(browser, repository_name)
	create_issue(browser, issue)
	sign_out_bitbucket(browser)
	sign_in_bitbucket(browser, user2)
	go_to_inbox(browser)
	verify_notification(browser, issue)
	delete_all_notifications(browser)
	sign_out_bitbucket(browser)

	log 'Cleanup'
	browser.close
rescue => e
	print 'F'
	failures.push("#{scenario_name} failed: #{e.message}\n#{e.backtrace}")
else
	print '.' unless $verbose_mode
end


log_heading 'Scenario: incorrect login test'
scenario_name = 'Incorrect login test'
num_tests += 1
begin
	log 'Setup'
	browser = Watir::Browser.new :ff
	user = User.new(user1_name, user1_email, user2_email)
	log 'Done initializing'

	log 'Scenario start'	
	log "Signing in user: #{user.name} (should fail)"
	browser.goto 'bitbucket.org'
	browser.link(text: /log in/i).click
	browser.text_field(id: 'id_username').set user.email
	browser.text_field(id: 'id_password').set user.password
	browser.form(id: 'login-form').submit
	
	log 'Verifying signin failure'
	if not browser.div(text: /invalid username\/email or password/i).exists?
		raise 'User is not given the correct error message upon illegal signin'
	elsif browser.link(text: /log out/i).exists?
		raise 'User illegally logged in!'
	end

	log 'Cleanup'
	browser.close
rescue => e
	print 'F'
	failures.push("#{scenario_name} failed: #{e.message}\n#{e.backtrace}")
else
	print '.' unless $verbose_mode
end


# Print all failures
log_heading 'Testing finished. Results.'
puts unless $verbose_mode
if failures.empty?
	puts 'All tests succeeded'
else
	failures.each { |failure| puts failure }
	if failures.length < num_tests then puts 'All other tests succeeded' end
end
	