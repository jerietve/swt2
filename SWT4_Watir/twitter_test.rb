require "watir-webdriver"

user1_email = 'user1.swt4@gmail.com'
user1_password = 'softwaretesting'

# Start Firefox
browser = Watir::Browser.new :ff

# Go to Twitter and log on
browser.goto "twitter.com"
browser.text_field(id: 'signin-email').set user1_email
browser.text_field(id: 'signin-password').set user1_password
browser.button(text: /sign in/i).click
# and.. fail because we need to enter a captcha