require "watir-webdriver"

user1_email = 'user1.swt4@gmail.com'
user1_password = 'softwaretesting'

# Start Firefox
browser = Watir::Browser.new :ff

# Go to Google Drive and log on
browser.goto "drive.google.com"
browser.text_field(id: 'Email').set user1_email
browser.text_field(id: 'Passwd').set user1_password
browser.button(id: 'signIn').click
# Skip the verification if necessary
if browser.button(id: 'save', value: /continue/).exists? browser.button(id: 'save', value: /continue/).click

# Create a new Document
#browser.button(text: 'create').click
# and.. fail because that menu is not controllable using htmls